DROP TABLE IF EXISTS Child;
DROP TABLE IF EXISTS Client;
DROP TABLE IF EXISTS Orders;
DROP TABLE IF EXISTS Users;

CREATE TABLE Users (
    ID INTEGER PRIMARY KEY,
    Username TEXT NOT NULL,
    Password TEXT NOT NULL
);

CREATE TABLE Child (
    ChildID int NOT NULL,
    Initials varchar(255),
    Gender varchar(255),
    Age int,
    ClothingSize varchar(255),
    Colours varchar(255),
    Characters varchar(255),

    PRIMARY KEY (ChildID)
);

CREATE TABLE Client (
    ClientID int NOT NULL,
    Name varchar(255),
    Email varchar(255),
    Number varchar(255),
    Address varchar(255),
    School varchar(255),
    SchoolPostcode varchar(255),
    ClothingGrant bool,
    ChildID int NOT NULL,

    PRIMARY KEY (ClientID),
    FOREIGN KEY (ChildID) REFERENCES Child(ChildID)
);

CREATE TABLE Orders (
    OrderID int NOT NULL,
    ClientID int NOT NULL,

    PRIMARY KEY (OrderID),
    FOREIGN KEY (ClientID) REFERENCES Client(ClientID)
);
